Oracle Cloud Platform
=====================

Account name: creasoft
username: creasoftdev@gmail.com / C[*]0!

## Create a VM instance
I chose: Ubuntu 20.04.2 LTS (GNU/Linux 5.4.0-1037-oracle x86_64)

export IP1="129.159.39.90"
export IP2="129.159.36.255"

```sh
# Download the certificate and
$ chmod 600 ~/devtools/oc-dev1-ssh.key
$ chmod 600 ~/devtools/oc-dev2-ssh.key

# Then to ssh, 
$ ssh ubuntu@${IP1} -i ~/devtools/oc-dev1-ssh.key
$ ssh ubuntu@${IP2} -i ~/devtools/oc-dev2-ssh.key
```
### Open up port
Go to Instance
1. Under "Primary VNIC", click on Subnet: subnet-20210303-2025
2. Under "Security Lists" click on "Default Security List for vcn-20210303-2025"
3. Click on "Add Ingress Rule"
  - source: 0.0.0.0/0 
  - port: 80
4. Do same for port 6443 (Kube api)

## Install Docker
https://docs.docker.com/engine/install/ubuntu/
https://docs.docker.com/engine/install/linux-postinstall/
$ sudo apt  install docker-compose


## Install Certificate
Follow the instructions for the VM to expose port 80:
`https://blogs.oracle.com/developers/free-ssl-certificates-in-the-oracle-cloud-using-certbot-and-lets-encrypt`

The installation of CertBot follow the following (as above will not work)
`https://certbot.eff.org/lets-encrypt/ubuntufocal-other`

sudo ufw allow 80/tcp

> NOTE: The server fails to listen (or accept request from outside). But docker does accept input from outside. Try docker instead

### CertBot with docker

Create a folder ~/letsencrypt and run the docker command as below:
```bash
# Create folder where the certs will be created
$ mkdir ~/letsencrypt

# Run certbot from docker
$ docker run --name=certbot -it -p 80:80 -v ~/letsencrypt:/etc/letsencrypt certbot/certbot:latest certonly --standalone -d knoesia-api-dev.betternia.com
```

The certificates should have been saved at `~/letsencrypt/live/knoesia-api-dev.betternia.com`

### Accessing server with SSH from GitLab CI
See: `https://gitlab.com/gitlab-examples/ssh-private-key/-/blob/master/.gitlab-ci.yml`


## Installing Kubernetes: 

### Install using 
Following: `https://rancher.com/docs/k3s/latest/en/installation/install-options/`

Opening port for k3s:
- https://github.com/k3s-io/k3s/issues/1280
- sudo ufw allow 6443/tcp && sudo ufw reload

sudo systemctl restart k3s

other utils installed: kubectl, crictl, k3s-killall.sh, k3s-uninstall.sh

https://github.com/collabnix/dockerlabs/blob/master/kubernetes/install/k3s/install-on-ubuntu01804.md

https://opensource.com/article/20/3/ssl-letsencrypt-k3s

> NOTE: Could not connect with kubectl

### Alternative Install Usin ksup
```sh
export IP2="129.159.36.255"
ssh ubuntu@${IP2} -i ~/devtools/oc-dev2-ssh-key-2021-03-21.key

k3sup install --ip $IP2 --user ubuntu --ssh-key ~/devtools/oc-dev2-ssh-key-2021-03-21.key --context kno-dev
```
export KUBECONFIG=/Users/ahnpy001/devtools/kubeconfig


### K3s Memory usage
Scratch  : MiB Mem :    976.9 total,    658.9 free,    211.9 used,
Dock+app : MiB Mem :    976.9 total,     94.2 free,    449.3 used
With  k3s: MiB Mem :    976.9 total,     65.7 free,    691.4 used,
With mk8s: MiB Mem :    976.9 total,     63.5 free,    678.4 used

Since Oracle free VMs are 1GM each, for the moment we will not use K8s.
