Authentication - Auth0
======================

Passport returns:

{
  "iss": "https://kine-dev.us.auth0.com/",
  "sub": "google-oauth2|112477426679212806404",
  "aud": [
    "https://kine-dev.us.auth0.com/api/v2/",
    "https://kine-dev.us.auth0.com/userinfo"
  ],
  "iat": 1614356784,
  "exp": 1614443184,
  "azp": "S5wQ9VXR08ZAybSzY2ow6DnqbmL1RHEZ",
  "scope": "openid profile"
}
