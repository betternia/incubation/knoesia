Technical Notes
===============

## Docker

### Useful tools:
- `dive` to inspect docker image
- `wait-for` to wait a service is listening: https://github.com/eficode/wait-for

> **Note** I was not able to have the docker-compose run from a subfolder, namely `./docker`. It fails when trying to build with the context `.`

Passing environments to `docker-compose` and docker are different.
To pass envs to `docker-compose` so that you can do substitutions in the compose file, you would do:
```
docker-compose --env-file ./config/.env.dev up
```
Alternatively you can pass by adding values in the `.env` file

To pass envs to docker service’s container, you can either define in the compose service
```yaml
  my_service:
    env_file:
      - ./Docker/api/api.env
    environment:
     - NODE_ENV=production
```

### Troubleshooting

When you get the following `no space` error as in "Cannot create container for service knoesia-backend: failed to copy files: userspace copy failed: write /var/lib/docker/volumes/...: no space left on device"
* See: https://stackoverflow.com/questions/31909979/docker-machine-no-space-left-on-device

Then,
```shell
# check the dangling volumes
$ docker volume ls -qf dangling=true
# Remove dangling volumes
$ docker volume rm $(docker volume ls -qf dangling=true)
```


## GitLab

Personal Access Token: https://docs.gitlab.com/ee/user/profile/personal_access_tokens.html

```bash
# Login into gitlab registry
$ docker login registry.gitlab.com -u ysahnpark -p ${CONTAINER_REGISTRY_TOKEN}
```

> CI when a variable is protected, it will only be applied on protected branches.


Gitlab CI ssh: https://gitlab.com/gitlab-examples/ssh-private-key/

## Postgres Provider
https://www.alwaysdata.com/en/register/
https://admin.alwaysdata.com/
Account name: creasoft / C[*]0!
creasoftdev@gmail.com  

## Kubectl Cluster
https://itnext.io/run-kubernetes-on-your-machine-7ee463af21a2

$ kubectl config get-contexts
$ kubectl get node -o wide

$ kubectl config use-context k3d-k3dcluster

### local K8s cluster with k3s & k3d

[K3s](https://k3s.io/) is a Lightweight Kubernetes created by Rancher. To facilitate the installation, the creators also provides [k3d](https://k3d.io/), k3s in docker.

For the installation just follow instructions in this [link](https://k3d.io/#installation).

For the cluster to expose a port, say 80 through the load balancer, follow instructions [here](https://gitlab.com/-/snippets/2086877). 

## Helm Tutorial
https://github.com/alexellis/helm3-expressjs-tutorial

Helm CI
https://sanderknape.com/2019/02/automated-deployments-kubernetes-gitlab/
https://sanderknape.com/2019/03/improving-kubernetes-deployments-helm/

```sh
$ helm install  --dry-run --debug {name} {chart-name}
```

Helm secret:
https://stackoverflow.com/questions/49928819/how-to-pull-environment-variables-with-helm-charts


## BUild time Security

### Snyk
https://www.gartner.com/reviews/market/application-security-testing/vendor/snyk/product/snyk/alternatives

Login to Snyk - with the developer's free account.
1. Go to [Integrations tab](https://app.snyk.io/org/creasoftdev/integrations)
2. Select the source control where your codebase is hosted, in my csae GitLab
3. Enter the "Personal access token"
  - To obtain the access token follow the [instructions here](https://docs.gitlab.com/ee/user/profile/personal_access_tokens.html)
4. A new page will open, click on the button "Add your GitLab project to Snyk"
  - Select the repositories to be scanned by Snyk and click the add button.

That's it! Check the identified vulnerabilities and address them.
https://app.snyk.io/org/creasoftdev/

### Other tools
https://github.com/renovatebot/renovate


## Monitoring: New Relic

### Installation 
```shell
# Linux
curl -Ls https://raw.githubusercontent.com/newrelic/newrelic-cli/master/scripts/install.sh | bash && sudo NEW_RELIC_API_KEY=${NR_API_KEY} NEW_RELIC_ACCOUNT_ID=${NR_ACCOUNT_ID} /usr/local/bin/newrelic install

# Docker
curl -Ls https://raw.githubusercontent.com/newrelic/newrelic-cli/master/scripts/install.sh | bash && NEW_RELIC_API_KEY=${NR_API_KEY} NEW_RELIC_ACCOUNT_ID=${NR_ACCOUNT_ID} /usr/local/bin/newrelic install
```

```sh
# Windows
[Net.ServicePointManager]::SecurityProtocol = 'tls12, tls'; (New-Object System.Net.WebClient).DownloadFile("https://github.com/newrelic/newrelic-cli/releases/latest/download/NewRelicCLIInstaller.msi", "$env:TEMP\NewRelicCLIInstaller.msi"); msiexec.exe /qn /i $env:TEMP\NewRelicCLIInstaller.msi | Out-Null; $env:NEW_RELIC_API_KEY='${NR_API_KEY}'; $env:NEW_RELIC_ACCOUNT_ID='${NR_ACCOUNT_ID}'; & 'C:\Program Files\New Relic\New Relic CLI\newrelic.exe' install
```

### Monitor Apps
1. Give an application a name
2. Download configuration (`yaml` file or `newrelic.js` file) and copy in the project root (not the source) directory.
3. Download/install agent
  - Java: `curl -O https://download.newrelic.com/newrelic/java-agent/newrelic-agent/current/newrelic-java.zip`
  - node: `yarn add newrelic`
  - typescript: `yarn add @types/newrelic --dev`
4. In the main JavaScript file, add this line on top: `require('newrelic');`
5. Restart the app
6. Go to https://one.newrelic.com/launcher


## Envoy

https://www.envoyproxy.io/docs/envoy/latest/start/quick-start/run-envoy

Generate key
```
# -nodes (short for no DES) if you don't want to protect  with password
openssl req -x509 -newkey rsa:4096 -keyout knoesis-test-net.key -out knoesis-test-net.crt -days 365 -nodes
```

```sh
$ docker run --rm -it \
      -v $(pwd)/envoy/envoy.local.yaml:/envoy-custom.yaml \
      -v $(pwd)/envoy/certs:/certs \
      -p 9901:9901 \
      -p 8443:8443 \
      envoyproxy/envoy-dev:a240824c376693b16ca8be51e435b95c42f3449f \
          -c /envoy-custom.yaml

```
