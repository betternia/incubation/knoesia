Charts
======

This directory includes helm chart for knoesia backend and values for the dependency: postgres.

## Installing knoesia in k8s
Here we assume you have a k8s cluster - for local test I recommend using k3d.

### Export variables
```sh
export AUTH0_ISSUER_URL=https://kine-dev.us.auth0.com/
export AUTH0_AUDIENCE=https://kine-dev.us.auth0.com/api/v2/

export DB_USERNAME=knoesia
export DB_PASSWORD=knoesiapwd
export DB_DATABASE_NAME=knoesia_k8s
export DB_DATABASE_URL=postgresql://${DB_USERNAME}:${DB_PASSWORD}@kno-postgres-postgresql:5432/${DB_DATABASE_NAME}?schema=public
```

### Install with helm (optionally append `--dry-run --debug`)
```sh
$ helm install knoesia-backend  knoesia-backend --set image.tag=latest \
  --set env.DB_USERNAME=${DB_USERNAME} \
  --set env.DB_PASSWORD=${DB_PASSWORD} \
  --set env.DATABASE_URL=${DB_DATABASE_URL} \
  --set env.AUTH0_ISSUER_URL=${AUTH0_ISSUER_URL} \
  --set env.AUTH0_AUDIENCE=${AUTH0_AUDIENCE}
```

### Running the migration
```sh
$ kubectl run knoesia-backend-migration --rm --tty -i --restart='Never' --namespace default --image registry.gitlab.com/betternia/incubation/knoesia/knoesia-backend:latest --env="DATABASE_URL=$DB_DATABASE_URL" --command -- npx prisma migrate deploy --preview-feature
```

### Port forward
```sh
$ kubectl --namespace default port-forward knoesia-backend-946cd8fb4-6swpm 3000:80
```


## APPENDIX: Installing postgres
Postgres is a dependency requirement for knoesia-backend to run

> bitnami repo must have been added `helm repo add bitnami https://charts.bitnami.com/bitnami`

See more configuration for postgresql: `https://github.com/bitnami/charts/tree/master/bitnami/postgresql`


### Helm Install
```sh
# Install by passing values in the args (change `install` to `upgrade` for subsequent execution)
$ helm install kno-postgres bitnami/postgresql \
  --set global.postgresql.postgresqlDatabase=${DB_DATABASE_NAME} \
  --set global.postgresql.postgresqlUsername=${DB_USERNAME} \
  --set global.postgresql.postgresqlPassword=${DB_PASSWORD} --dry-run --debug 

$ helm install kno-postgres bitnami/postgresql \
  --set postgresqlDatabase=${DB_DATABASE_NAME} \
  --set postgresqlUsername=${DB_USERNAME} \
  --set postgresqlPassword=${DB_PASSWORD} --dry-run --debug 

# Install by pass file with environment values
$ helm install kno-postgres bitnami/postgresql -f postgres-values.yaml --dry-run --debug 
```

Once postgres install, the NOTE will be printed in the console. Follow the instructions tu run the client or the run the port-forwarding proxy.

> NOTE: The database was not crated, so I had to create the database manually.  NOTE2: The volume is persistent, need to figure out the configuration of the persistent volume. 


### Test postgres with client
```
kubectl run kno-postgres-postgresql-client --rm --tty -i --restart='Never' --namespace default --image docker.io/bitnami/postgresql:11.11.0-debian-10-r24 --env="PGPASSWORD=$POSTGRES_PASSWORD" --command -- psql --host kno-postgres-postgresql -U knoesia -d knoesia_k8s -p 5432
```

## APPENDIX: Adding repo
```sh
# helm repo add bitnami https://charts.bitnami.com/bitnami
$ helm search repo bitnami
$ helm install my-release bitnami/<chart>
```
