import { Pageable } from './pagination';

interface PrismaQueryParams {
  skip?: number;
  take?: number;
  where?: any;
  cursor?: any;
  orderBy?: any;
}

export function pageableToPrismaParams(pageable:Pageable, where?: any): PrismaQueryParams {
  
  const orderBy = {};
  if (pageable.sort?.orders) {
    for (const order of pageable.sort?.orders) {
      orderBy[order.property] = order.direction.toString();
    }
  }
  
  const findParams = {
    skip: (pageable.page - 0) * pageable.size,
    take: pageable.size,
    where: where,
    orderBy: orderBy
  }
  return findParams;
}
