import { Module } from '@nestjs/common';
import { AppController } from './app.controller';
import { AppService } from './app.service';
import { UserModule } from './user/user.module';
import { NoemaModule } from './noema/noema.module';
import { AuthzModule } from './authz/authz.module';

@Module({
  imports: [UserModule, NoemaModule, AuthzModule],
  controllers: [AppController],
  providers: [AppService],
})
export class AppModule {}
