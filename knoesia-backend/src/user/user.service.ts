import { Injectable } from '@nestjs/common';
import { PrismaService } from '../prisma.service';
import {
  User,
  UserAccount,
  Prisma
} from '@prisma/client';

import jwtDecode, { JwtPayload } from 'jwt-decode';

import { UserAccountDto } from './useraccount.dto';
import { UserDto } from './user.dto';

@Injectable()
export class UserService {
  constructor(private prisma: PrismaService) { }

  async account(userAccountWhereUniqueInput: Prisma.UserAccountWhereUniqueInput): Promise<UserAccount | null> {
    return this.prisma.userAccount.findUnique({
      where: userAccountWhereUniqueInput,
      include: {
        user: true
      }
    });
  }

  async user(userWhereUniqueInput: Prisma.UserWhereUniqueInput): Promise<User | null> {
    return this.prisma.user.findUnique({
      where: userWhereUniqueInput,
    });
  }

  async users(params: {
    skip?: number;
    take?: number;
    cursor?: Prisma.UserWhereUniqueInput;
    where?: Prisma.UserWhereInput;
    orderBy?: Prisma.UserOrderByInput;
  }): Promise<User[]> {
    const { skip, take, cursor, where, orderBy } = params;
    return this.prisma.user.findMany({
      skip,
      take,
      cursor,
      where,
      orderBy,
    });
  }

  async createUser(data: Prisma.UserCreateInput): Promise<User> {
    if (!data.id) {
      throw Error('User id cannot be null');
    }
    return this.prisma.user.create({
      data,
    });
  }


  /**
   * Checks whether or not a UserAccount that matches source, sourceAccountId exists.
   * It it does, returned the matching UserAccount.
   * Otherwise, 
   * 1. If userId was provided, retrieve such user, else create new user.
   * 2. Create account and associate with the user.
   * 
   * NOTE: You can have one user with multiple accounts, e.g. one for Facebook, another for Google, etc.
   * 
   * @param account 
   * @param userId  If userId is provided, the account will be associated to this user. 
   */
  async registerAccount(
    account: UserAccountDto
  ): Promise<UserAccountDto> {

    const existingAccount = await this.prisma.userAccount.findUnique({
      where: {
        source_sourceAccountId: {
          source: account.source,
          sourceAccountId: account.sourceAccountId,
        }
      },
      include: {
        user: true
      }
    });

    if (existingAccount) {
      return existingAccount;
    }

    // UserAccount match not found: 
    // If userId was provided associate with an existing user, otherwise, create a new user

    const userInput: Prisma.UserCreateInput = account.user as Prisma.UserCreateInput;

    account.user.dateCreated = new Date();
    
    let user: User = account.user?.uid ? 
      await this.prisma.user.findUnique({
        where: { uid: account.user?.uid }
      }) 
      : await this.createUser(userInput);

    if (!user) {
      throw Error('User not found');
    }

    const userAccountInput: Prisma.UserAccountCreateInput = account as Prisma.UserAccountCreateInput;

    userAccountInput.dateCreated = new Date();
    userAccountInput.user = {
      connect: { uid: user.uid }
    };

    let createdUserAccount = await this.prisma.userAccount.create({
      data: userAccountInput,
    });
    let createdUserAccountDto = createdUserAccount as UserAccountDto;
    createdUserAccountDto.user = user;
    return createdUserAccountDto;
  }

  async updateUser(params: {
    where: Prisma.UserWhereUniqueInput;
    data: Prisma.UserUpdateInput;
  }): Promise<UserDto> {
    const { data } = params;
    const where: Prisma.UserWhereUniqueInput = params.where ? params.where : { uid: data.uid as string }
    return this.prisma.user.update({
      data,
      where,
    });
  }

  async deleteUserByUid(userUid: string): Promise<User> {

    await this.prisma.userAccount.deleteMany({ where: { userUid: userUid } })

    // {uid: createdAccount.userUid}
    return this.prisma.user.delete({
      where: { uid: userUid }
    });
  }

  /**
   * Returns UserAccountDto from idToken.
   * The ID Token format is based on Auth0+Google.
   * TODO: Verify other providers
   * 
   * @param idToken 
   * @returns 
   */
  userAccountFromIdToken(idToken: string): UserAccountDto {
    const idTokenJson = jwtDecode<JwtPayload>(idToken);
    return {
      source: idTokenJson.iss,
      sourceAccountId: idTokenJson.sub,
      syncDate: (new Date()).toJSON(),
      user: {
        id: idTokenJson['email'],
        givenName: idTokenJson['given_name'],
        familyName: idTokenJson['family_name']
      }
    }
  }
}
