import { Test, TestingModule } from '@nestjs/testing';
import { PrismaService } from '../prisma.service';
import { UserService } from './user.service';

import {
  newAccountDto, newUser
} from './user.test.utils';

describe('UserService', () => {
  let app: TestingModule;
  let userService: UserService;

  beforeAll(async () => {
    app = await Test.createTestingModule({
      providers: [UserService, PrismaService],
    }).compile();

    userService = app.get<UserService>(UserService);
  });

  afterAll(async () => {
    await app.close();
  });


  describe('root', () => {
    it('WHEN createUser, THEN user is in db', async () => {
      const createdUser = await userService.createUser(newUser('T1'));
      expect(createdUser).not.toBeNull();

      // Remove
      await userService.deleteUserByUid(createdUser.uid);
    });

    it('WHEN registerAccount, THEN user and account is in db', async () => {
      const createdAccount = await userService.registerAccount(newAccountDto('test', 'A1', 'T12'));
      expect(createdAccount).not.toBeNull();

      // Find user
      const foundUser = await userService.user({uid: createdAccount.userUid});
      expect(foundUser).not.toBeNull();

      // Remove
      await userService.deleteUserByUid(createdAccount.userUid);  // Account will delete cascade
    });

    it('WHEN userAccountFromIdToken called with proper token, THEN returns account', () => {
      const idToken = "eyJhbGciOiJSUzI1NiIsInR5cCI6IkpXVCIsImtpZCI6ImVDcDVudHdqeWRZZUVCQUQ4SlNzcSJ9.eyJnaXZlbl9uYW1lIjoiWW91bmcgU3VrIiwiZmFtaWx5X25hbWUiOiJBaG4iLCJuaWNrbmFtZSI6ImtpbmVhcnQiLCJuYW1lIjoiWW91bmcgU3VrIEFobiIsInBpY3R1cmUiOiJodHRwczovL2xoMy5nb29nbGV1c2VyY29udGVudC5jb20vYS0vQU9oMTRHZ3hMV1hDUS16UVhkR0FyNE9sVnFJU2t6MFVHaGw0bTJHVjdnZUVMUT1zOTYtYyIsImxvY2FsZSI6ImVuIiwidXBkYXRlZF9hdCI6IjIwMjEtMDYtMDhUMjI6Mzg6MTYuMDAyWiIsImVtYWlsIjoia2luZWFydEBnbWFpbC5jb20iLCJlbWFpbF92ZXJpZmllZCI6dHJ1ZSwiaXNzIjoiaHR0cHM6Ly9raW5lLWRldi51cy5hdXRoMC5jb20vIiwic3ViIjoiZ29vZ2xlLW9hdXRoMnwxMTI0Nzc0MjY2NzkyMTI4MDY0MDQiLCJhdWQiOiJTNXdROVZYUjA4WkF5YlN6WTJvdzZEbnFibUwxUkhFWiIsImlhdCI6MTYyMzE5MjUxMiwiZXhwIjoxNjIzMjI4NTEyfQ.jRWbUetyAf3qupRXCug76gofHQ9yLakp1fEDM5pUEoVK_vfURgOXw8cyMP2GfZ8TQh0m3BJzVrW8-GkhxrJOcNaXHJbb50-0naS-uVhtqIOQdBqNOY0U-Kyc8HUIHyTuVkmKBGIx0TZ5FwOTVaWwb5gcutsvUu8gmNhp_Fdjd68TJ_Sqqx6dMrR1ghFHG3epNKPkS4c9uj5zuT70tp3VJ4wSMiU_sGsIzHCxdfa6ghyG8wB7ufUwOJDACr_hbJ7Py2wLwoYRq_BWpDM8vGCZ6xajY11u9X6Q-hgTXjwf8tR4Jq9o8F9fl82Bgyv1BVFhqrjnNrSglSZvdf6JwscqpA";
      const account = userService.userAccountFromIdToken(idToken);

      expect(account.source).toEqual('https://kine-dev.us.auth0.com/');
      expect(account.sourceAccountId).toEqual('google-oauth2|112477426679212806404');
      expect(account.user.id).toEqual('kineart@gmail.com');
      expect(account.user.givenName).toEqual('Young Suk');
      expect(account.user.familyName).toEqual('Ahn');

    });

  });

});
