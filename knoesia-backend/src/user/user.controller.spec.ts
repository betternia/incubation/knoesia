import * as request from 'supertest';
import { INestApplication } from '@nestjs/common';
import { Test, TestingModule } from '@nestjs/testing';
import { PrismaService } from '../prisma.service';
import { UserController } from './user.controller';
import { UserService } from './user.service';

import { createAccount } from './user.test.utils';

describe('UserController', () => {
  let app: INestApplication;
  let userController: UserController;
  let userService: UserService;

  beforeAll(async () => {
    const moduleRef: TestingModule = await Test.createTestingModule({
      controllers: [UserController],
      providers: [PrismaService, UserService],
    }).compile();

    userService = moduleRef.get<UserService>(UserService);
    userController = moduleRef.get<UserController>(UserController);

    app = moduleRef.createNestApplication();
    await app.init();
  });

  afterAll(async () => {
    await app.close();
  });

  describe('endpoints', () => {
    it('[GET] /users/:xid', async () => {
      const account = await createAccount(userService, 'CtlrTest', 'T1');
      // console.log('IN:' + JSON.stringify(account, null, 2));

      await request(app.getHttpServer())
        .get(`/users/${account.userUid}`)
        .expect(200)
        .expect(function (res) {
           expect(res.body.uid).toBe(account.userUid);
        });
      
      await userService.deleteUserByUid(account.userUid);
    });

    it('[GET] /accounts/:xid', async () => {
      const account = await createAccount(userService, 'CtlrTest', 'T2');

      await request(app.getHttpServer())
        .get(`/accounts/${account.uid}`)
        .expect(200)
        .expect(function (res) {
          let act = {...res.body} 
          act.dateCreated = new Date(act.dateCreated);
          act.lastAccessDate = new Date(act.lastAccessDate);
          act.syncDate = new Date(act.syncDate);
          act.user.dateCreated = new Date(act.user.dateCreated);
          expect(act).toEqual(account);
        });
      await userService.deleteUserByUid(account.userUid);
    });
  });
});
