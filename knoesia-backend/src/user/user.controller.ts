import {
  Controller, Logger,
  Get,
  Param,
  Post,
  Body,
  Put,
  Delete,
  Req,
  UseGuards,
  HttpException,
  HttpStatus,
} from '@nestjs/common';
import { AuthGuard } from '@nestjs/passport';
import { ApiTags } from '@nestjs/swagger';

import { Prisma, User, UserAccount } from '@prisma/client';
import { UserDto } from './user.dto';
import { UserService } from './user.service';
import { UserAccountDto } from './useraccount.dto';


@Controller()
@ApiTags('user')
export class UserController {
  private readonly logger = new Logger(UserController.name);

  constructor(
    private readonly userService: UserService
  ) { }

  @UseGuards(AuthGuard('jwt'))
  @Get('/my/account')
  async getMyAccount(@Req() req: any): Promise<UserAccountDto> {

    const user = <UserAccount>req.user;
    this.logger.debug({ op: 'getMyAccount', user });
    let account = await this.userService.account({ uid: user.uid });
    if (!account) {
      throw new HttpException('NotFound', HttpStatus.NOT_FOUND);;
    }
    return account;
  }

  @Get('users/:xid')
  async getUserById(@Param('xid') xid: string): Promise<UserDto> {
    this.logger.debug({ op: 'getUserById', param: xid });
    let user = await this.userService.user({ id: xid }) ?? await this.userService.user({ uid: xid });
    return user;
  }

  @Get('accounts/:uid')
  async getUserAccountByUid(@Param('uid') uid: string): Promise<UserAccountDto> {
    this.logger.debug({ op: 'getUserAccountByUid', param: uid });
    let account = await this.userService.account({ uid })
    this.logger.debug({ op: 'getUserAccountByUid', 'FOUND': account });
    return account;
  }

  /**
   * Checks whether or not a UserAccount that matches source, sourceAccountId exists.
   * It it does, returned the matching UserAccount.
   * Otherwise, 
   * 1. If user was provided, retrieve such user, else create new user.
   * 2. Create account and associate with the user.
   */
  @UseGuards(AuthGuard('jwt'))
  @Post('register-account')
  /** Sample input
{
    "account": {
        "source": "https://kine-dev.us.auth0.com/",
        "sourceAccountId": "google-oauth2|112477426679212806404",
        "syncDate": "2021-02-19T23:10:55.551Z",
        "user": {
          "id": "hkd",
          "givenName": "MSW Kil Dong",
          "familyName": "MSW Hong"
        }
    },
}
   */
  async registerAccount(
    @Body() postData: {
      idToken?: string,
      account?: UserAccountDto,
      userUid?: string
    }
  ): Promise<UserAccountDto> {
    this.logger.debug({ op: 'registerAccount', param: postData });

    if (!postData.idToken && !postData.account) {
      throw Error('Either idToken or account must be provided.');
    }

    const account = postData.idToken ?
      this.userService.userAccountFromIdToken(postData.idToken) : postData.account;
    if (postData.userUid) 
      account.user.uid = postData.userUid;
    return this.userService.registerAccount(account);
  }
}
