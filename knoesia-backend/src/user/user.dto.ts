import { UserAccountDto } from './useraccount.dto'

// From UserUncheckedCreateInput
export class UserDto {
  sid?: number
  uid?: string
  dateCreated?: Date | string
  dateModified?: Date | string | null
  id: string
  givenName: string
  familyName: string
  additionalName?: string | null
  alternateName?: string | null
  address?: string | null
  description?: string | null
  birthDate?: string | null
  birthPlace?: string | null
  image?: string | null
  url?: string | null
  locale?: string | null
  emails?: string[]
  accounts?: UserAccountDto[]
}
