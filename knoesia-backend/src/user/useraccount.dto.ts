import { UserDto } from './user.dto'

// From UserAccountUncheckedCreateInput
export class UserAccountDto {
  sid?: number
  uid?: string
  dateCreated?: Date | string
  dateModified?: Date | string | null
  source: string
  sourceAccountId: string
  syncDate?: Date | string
  sourceProfile?: string | null
  lastAccessDate?: Date | string
  passcode?: string | null
  loginFailCount?: number | null
  userUid?: string
  user?: UserDto
}
