import {
  Prisma, User, UserAccount
} from '@prisma/client';
import { UserService } from './user.service';
import { UserAccountDto } from './useraccount.dto';

export async function createAccount(
  userService: UserService, 
  source: string, idPrefix: string
): Promise<UserAccountDto> {
  const rands = randStr();
  const createdAccount = await userService.registerAccount(
    newAccountDto(source, `${idPrefix}-${rands}`, `${idPrefix}-${rands}`));
  return createdAccount;
}

export async function deleteUser(
  userService: UserService, 
  account: UserAccountDto
): Promise<User> {
  const deletedUser = await userService.deleteUserByUid(account.userUid);
  return deletedUser;
}

// Simple random string generator (9 chars)
export function randStr(): string {
  return Math.random().toString(36).substr(2, 9);
} 

// Auxiliary functions:
export function newAccount(source: string, accountId: string): Prisma.UserAccountCreateInput {
  return {
    source: source,
    sourceAccountId: accountId,
    sourceProfile: '',
    user: undefined
  }
}

export function newUser(name: string, familyName: string='Tester'): Prisma.UserCreateInput {
  return {
    id: name,
    givenName: name,
    familyName: familyName
  }
}

export function newAccountDto(source: string, accountId: string, name: string, familyName: string='Tester'): UserAccountDto {
  return {
    source: source,
    sourceAccountId: accountId,
    sourceProfile: '',
    user: {
      id: name,
      givenName: name,
      familyName: familyName
    }
  }
}
