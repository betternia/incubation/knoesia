const newrelic = require('newrelic');
import { NestFactory } from '@nestjs/core';
import { SwaggerModule, DocumentBuilder } from '@nestjs/swagger';

import { AppModule } from './app.module';
import * as AppMeta from './app.meta.json'

const port = parseInt(process.env.SERVER_PORT, 10) || 3000;
// This web path will open the OpenAPI documentation page 
const APIDOC_PATH = 'apidoc'; 

async function bootstrap() {
  const app = await NestFactory.create(AppModule);
  app.enableCors();

  const options = new DocumentBuilder()
    .setTitle(AppMeta.APP_NAME)
    .setDescription(`The ${AppMeta.APP_NAME} API description`)
    .setVersion(AppMeta.APP_VERSION)
    .build();
  const document = SwaggerModule.createDocument(app, options);
  SwaggerModule.setup(APIDOC_PATH, app, document);

  await app.listen(port);
  console.log('Listening port:' + port);
}
bootstrap();
