import { CanActivate, ExecutionContext } from '@nestjs/common';

export class DummyGuard implements CanActivate {
  canActivate(
    context: ExecutionContext,
  ): boolean | Promise<boolean>  {
    context.switchToHttp().getRequest().user = {};
    return true;
  }
}
