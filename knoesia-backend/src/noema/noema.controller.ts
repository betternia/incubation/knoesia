import {
  Controller, Logger, UseGuards,
  Get, Post, Body, Put, Delete,
  Param,
  Req,
  Query,
} from '@nestjs/common';
import { AuthGuard } from '@nestjs/passport';
import { ApiTags, ApiOkResponse, ApiExtraModels, getSchemaPath } from '@nestjs/swagger';

import { Prisma, UserAccount, Noema } from '@prisma/client';
import { ArrayPage, Page, Pageable } from '../foundation/pagination';
import { NoemaCreateDto } from './noema-create.dto';
import { NoemaDto } from './noema.dto';

import { NoemaService } from './noema.service';


@Controller()
@ApiTags('noema')
@ApiExtraModels(Page)
export class NoemaController {

  private readonly logger = new Logger(NoemaController.name);

  constructor(
    private readonly noemaService: NoemaService
  ) { }

  @UseGuards(AuthGuard('jwt'))
  @Get('noemas-info')
  async getEndpointInfo(): Promise<any> {
    return { version: '0.1' }
  }

  @Get('noemas/:uid')
  async getNoemaById(@Param('uid') uid: string): Promise<NoemaDto> {
    this.logger.debug({ op: 'getNoemaById', param: uid });
    return await this.noemaService.noema({ uid });
  }

  @UseGuards(AuthGuard('jwt'))
  @Get('noemas')
  @ApiOkResponse({
    schema: {
      allOf: [
        { $ref: getSchemaPath(Page) },
        {
          properties: {
            content: {
              type: 'array',
              items: { $ref: getSchemaPath(NoemaDto) },
            },
          },
        },
      ],
    },
  })
  async query(@Query() queryParams: Map<string, string>, @Req() req: any): Promise<ArrayPage<NoemaDto>> {
    const userAccount = <UserAccount>req.user;
    this.logger.debug({ op: 'query', queryParams });

    let pageable = Pageable.buildFromQuery(req.query);
    // console.log('pageable: ' + JSON.stringify(pageable, null, 2));
    let where = {
      userUid: userAccount.userUid
    }
    return await this.noemaService.queryNoema({ pageable, where });
  }

  // TODO: schema checker
  @UseGuards(AuthGuard('jwt'))
  @Post('noemas')
  async add(@Body() noema: NoemaCreateDto, @Req() req: any): Promise<NoemaDto> {
    const userAccount = <UserAccount>req.user;
    this.logger.debug({ op: 'add', noema, userAccount });
    const noemaInput = noema as Prisma.NoemaCreateInput;
    return await this.noemaService.createNoema(noemaInput, userAccount, { tags: noema.tags });
  }

  // TODO: schema checker
  @UseGuards(AuthGuard('jwt'))
  @Put('noemas/:uid')
  async update(@Param('uid') uid: string, @Body() noema: NoemaCreateDto, @Req() req: any): Promise<NoemaDto> {
    const userAccount = <UserAccount>req.user;
    noema.uid = uid;
    this.logger.debug({ op: 'update', noema, userAccount });
    return await this.noemaService.updateNoema({ uid: noema.uid }, noema, { tags: noema.tags });
  }

  // TODO: schema checker
  @UseGuards(AuthGuard('jwt'))
  @Delete('noemas/:uid')
  async delete(@Param('uid') uid: string, @Req() req: any): Promise<NoemaDto> {
    const userAccount = <UserAccount>req.user;
    this.logger.debug({ op: 'delete', uid, userAccount });
    return await this.noemaService.deleteNoema({ uid: uid });
  }
}
