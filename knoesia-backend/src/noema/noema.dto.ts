import { NoemaTagDto } from './noematag.dto'
import { NoemaAttachmentDto } from './noemaattachment.dto'

// From NoemaUncheckedCreateInput
export class NoemaDto {
  sid: number
  uid?: string
  dateCreated?: Date | string
  dateModified?: Date | string | null
  status?: number | null
  userUid?: string | null
  kind: string
  eventDate: Date | string
  title?: string | null
  content: string
  highlighted?: boolean | null
  mood?: number | null
  category?: string | null
  activity?: string | null
  location?: string | null
  weather?: string | null
  weatherTemperature?: number | null
  trunkUid?: string | null
  tags?: NoemaTagDto[]
  attachments?: NoemaAttachmentDto[]
  branches?: string[]
}
