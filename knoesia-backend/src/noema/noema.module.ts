import { Module } from '@nestjs/common';
import { PrismaService } from '../prisma.service';
import { NoemaController } from './noema.controller';
import { NoemaService } from './noema.service';

@Module({
  controllers: [NoemaController],
  providers: [PrismaService, NoemaService]
})
export class NoemaModule {}
