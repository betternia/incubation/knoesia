import * as request from 'supertest';
import { INestApplication } from '@nestjs/common';
import { Test, TestingModule } from '@nestjs/testing';
import { PrismaService } from '../prisma.service';
import { AuthGuard } from '@nestjs/passport';

import { NoemaController } from './noema.controller';
import { NoemaService } from './noema.service';

import { deleteUser, createAccount } from '../user/user.test.utils';
import { UserService } from '../user/user.service';
import { UserAccount } from '@prisma/client';
import { DummyGuard } from '../utils/test.utils';

describe('NoemaController', () => {
  let app: INestApplication;
  let userService: UserService;
  let noemaService: NoemaService;
  let controller: NoemaController;

  beforeAll(async () => {

    const module: TestingModule = await Test.createTestingModule({
      controllers: [NoemaController],
      providers: [PrismaService, UserService, NoemaService],
    })
      // NOTE: Overriding the guard allows bypass the guard during test 
      .overrideGuard(AuthGuard('jwt')).useClass(DummyGuard)
      .compile();

    userService = module.get<UserService>(UserService);
    noemaService = module.get<NoemaService>(NoemaService);
    controller = module.get<NoemaController>(NoemaController);

    app = module.createNestApplication();
    await app.init();
  });

  afterAll(async () => {
    await app.close();
  });

  it('should be defined', () => {
    expect(controller).toBeDefined();
  });

  describe('endpoints', () => {
    it('[GET] /noemas/:uid', async () => {

      const account = await createAccount(userService, 'NoemaCtlrTest', 'T1') as UserAccount;
      const createdNoema = await noemaService.createNoema(newNoema('Noema Controller test'), account, { tags: ['a', 'b'] });

      const url = `/noemas/${createdNoema.uid}`;
      await request(app.getHttpServer())
        .get(url)
        .expect(200)
        .expect(function (res) {
          expect(res.body.content).toBe(createdNoema.content);
        });

      await noemaService.deleteNoema({ uid: createdNoema.uid });
      await userService.deleteUserByUid(account.userUid);
    });

    it('[POST] /noemas', async () => {
      let createdNoema;
      const url = `/noemas`;
      await request(app.getHttpServer())
        .post(url)
        .send(newNoema('Controller test'))
        .expect(201)
        .expect(function (res) {
          createdNoema = res.body;
          // console.log('Created: ', createdNoema);
          expect(createdNoema).not.toBeNull();
          expect(createdNoema.title).toEqual('Controller test');
        });

      await noemaService.deleteNoema({ uid: createdNoema.uid });
    });

    it('[PUT] /noemas', async () => {
      const account = await createAccount(userService, 'NoemaCtlrTest', 'T1') as UserAccount;
      const createdNoema = await noemaService.createNoema(newNoema('Noema Controller test'), account, { tags: ['a', 'b'] });

      const url = `/noemas/${createdNoema.uid}`;
      await request(app.getHttpServer())
        .put(url)
        .send(newNoema('Modified by test'))
        .expect(200)
        .expect(function (res) {
          const updatedNoema = res.body;
          console.log('** originalNoema: ', JSON.stringify(createdNoema, null, 2));
          console.log('** updatedNoema: ', JSON.stringify(updatedNoema, null, 2));
          expect(updatedNoema).not.toBeNull();
          expect(updatedNoema.userUid).toEqual(account.userUid);
          expect(updatedNoema.title).toEqual('Modified by test');
          expect(updatedNoema.dateModified).not.toBeNull();
        });

      await noemaService.deleteNoema({ uid: createdNoema.uid });
      await userService.deleteUserByUid(account.userUid);
    });

    function newNoema(title: string): any {
      return {
        "status": 0,
        "kind": "test",
        "eventDate": (new Date()).toJSON(),
        "title": title,
        "content": "Testing",
        "highlighted": true,
        "mood": 2,
        "category": "string",
        "activity": "string",
        "location": "string",
        "weather": "string",
        "weatherTemperature": 23,
        "tags": ["test", "unit"]
      }
    }
  });
});
