import { Prisma } from '@prisma/client';

// TODO: turn this into class for the Nestjs OpenAPI to inspect and generate code
export interface NoemaCreateDto extends Prisma.NoemaCreateManyInput {

  tags?: string[],
  attachments?: Prisma.NoemaAttachmentCreateManyInput[]
}
