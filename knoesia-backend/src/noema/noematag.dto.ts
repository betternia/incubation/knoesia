
// From NoemaTagUncheckedCreateInput
export class NoemaTagDto {
  sid?: number
  uid?: string
  type: string
  name: string
  noemaUid?: string | null
}
