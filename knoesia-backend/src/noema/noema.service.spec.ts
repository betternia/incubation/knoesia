import { Test, TestingModule } from '@nestjs/testing';
import { PrismaService } from '../prisma.service';
import { UserService } from '../user/user.service';
import {
  Prisma,
  User,
  UserAccount,
  Noema
} from '@prisma/client';
import { deleteUser, createAccount } from '../user/user.test.utils';

import { newNoema, newNoemaTag } from './noema.test.utils'

// Service under test
import { NoemaService } from './noema.service';
import { Pageable } from '../foundation/pagination';
import { NoemaDto } from './noema.dto';
import { NoemaCreateDto } from './noema-create.dto';

describe('NoemaService', () => {
  let app: TestingModule;
  let userService: UserService;
  let noemaService: NoemaService;

  beforeAll(async () => {
    app = await Test.createTestingModule({
      providers: [NoemaService, UserService, PrismaService],
    }).compile();

    noemaService = app.get<NoemaService>(NoemaService);
    userService = app.get<UserService>(UserService);
  });

  afterAll(async () => {
    await app.close();
  });

  describe('CRUD', () => {
    it('WHEN create, query, update, delete Noema, THEN ops work as expected', async () => {

      const account = await createAccount(userService, 'test-noema', 't1') as UserAccount;

      // Create
      const createdNoema = await noemaService.createNoema(newNoema('This is a test'), account, { tags: ['a', 'b'] });
      expect(createdNoema).not.toBeNull();

      // Query
      const foundNoemas = await noemaService.noemas({ where: { userUid: account.userUid } });
      expect(foundNoemas.length).toEqual(1);
      expect(foundNoemas[0].userUid).toEqual(account.userUid);
      expect(foundNoemas[0]['tags'].length).toEqual(2);

      // Update
      const data: NoemaCreateDto = {
        content: 'Modified content',
        kind: 'test',
        eventDate: new Date()
      };
      const updatedNoema = await noemaService.updateNoema(
        { uid: foundNoemas[0].uid },
        data,
        { tags: ['b', 'c', 'd'] }
      );

      // Query updated
      const foundNoema = await noemaService.noema({ uid: foundNoemas[0].uid });
      expect(foundNoema.uid).toEqual(createdNoema.uid);
      expect(foundNoema.content).toEqual('Modified content');
      expect(foundNoema.userUid).toEqual(account.userUid);
      expect(foundNoema['tags'].length).toEqual(3);

      await noemaService.deleteNoema({ uid: createdNoema.uid });

      // Query deleted
      const foundDeletedNoema = await noemaService.noema({ uid: foundNoema.uid });
      expect(foundDeletedNoema).toBeNull();


      await deleteUser(userService, account);
    });

    it('WHEN query, THEN return page', async () => {
      const account = await createAccount(userService, 'test-noema', 't2') as UserAccount;

      // Create
      let noemas: NoemaDto[] = [];
      for (let i = 0; i < 10; i++) {
        noemas.push(
          await noemaService.createNoema(newNoema(`N-${i}`), account, { tags: ['tag'] })
        );
      }
      const pageable = new Pageable(0, 4, ['eventDate:desc']);
      const noemasPage = await noemaService.queryNoema({ where: { userUid: account.userUid }, pageable });
      const foundNoemaUids = noemasPage.content.map(x => x.uid);

      const reversed = noemas.slice(6).reverse();
      const expectedUids = reversed.map(x => x.uid);

      // console.debug('**:' + JSON.stringify(noemasPage, null, 2));
      // console.debug('**:' + JSON.stringify(noemas, null, 2));

      expect(noemasPage.size).toEqual(4);
      expect(noemasPage.totalElements).toEqual(10);
      expect(noemasPage.number).toEqual(0);
      expect(noemasPage.totalPages).toEqual(3);

      expect(noemasPage.content.length).toEqual(4);
      expect(foundNoemaUids).toEqual(expectedUids);

      for (let n of noemas) {
        await noemaService.deleteNoema({ uid: n.uid });
      }
      await deleteUser(userService, account);
    });
  });

  describe('helper functions', () => {
    describe('tagsDiff', () => {
      it('WHEN originalTags and newTags have differences, THEN return added and removed', async () => {

        const a: Prisma.NoemaTagCreateInput[] = [
          newNoemaTag('a', 'UID1'),
          newNoemaTag('b'),
          newNoemaTag('c'),
        ];
        const b: Prisma.NoemaTagCreateInput[] = [
          newNoemaTag('b'),
          newNoemaTag('c'),
          newNoemaTag('d'),
        ];

        const diff = noemaService.tagsDiff(a, b);
        expect(diff.removed.map(x => x.name).sort()).toEqual(['a']);
        expect(diff.added.map(x => x.name).sort()).toEqual(['d']);
      });

      it('WHEN originalTags and newTags are same, THEN return empty added and removed', async () => {

        const a: Prisma.NoemaTagCreateInput[] = [
          newNoemaTag('a', 'UID1'),
          newNoemaTag('b'),
        ];
        const b: Prisma.NoemaTagCreateInput[] = [
          newNoemaTag('a'),
          newNoemaTag('b'),
        ];

        const diff = noemaService.tagsDiff(a, b);
        expect(diff.removed.map(x => x.name).sort()).toEqual([]);
        expect(diff.added.map(x => x.name).sort()).toEqual([]);
      });

      it('WHEN originalTags is superset of newTags, THEN return empty added', async () => {

        const a: Prisma.NoemaTagCreateInput[] = [
          newNoemaTag('a', 'UID1'),
          newNoemaTag('b'),
        ];
        const b: Prisma.NoemaTagCreateInput[] = [
          newNoemaTag('b'),
        ];

        const diff = noemaService.tagsDiff(a, b);
        expect(diff.removed.map(x => x.name).sort()).toEqual(['a']);
        expect(diff.added.map(x => x.name).sort()).toEqual([]);
      });

      it('WHEN originalTags is empty of newTags, THEN return added only', async () => {

        const a: Prisma.NoemaTagCreateInput[] = [];
        const b: Prisma.NoemaTagCreateInput[] = [
          newNoemaTag('a'),
          newNoemaTag('b'),
        ];

        const diff = noemaService.tagsDiff(a, b);
        expect(diff.removed.map(x => x.name).sort()).toEqual([]);
        expect(diff.added.map(x => x.name).sort()).toEqual(['a', 'b']);
      });
    });
  });
});
