import { Injectable, Logger } from '@nestjs/common';
import { PrismaService } from '../prisma.service';
import {
  Noema,
  NoemaTag, NoemaAttachment,
  Prisma, PrismaPromise,
  UserAccount
} from '@prisma/client';

import { Pageable, ArrayPage, Page } from '../foundation/pagination';
import { pageableToPrismaParams } from '../foundation/prisma.adapter';
import { NoemaDto } from './noema.dto';
import { NoemaCreateDto } from './noema-create.dto';


@Injectable()
export class NoemaService {
  private readonly logger = new Logger(NoemaService.name);

  constructor(private prisma: PrismaService) { }

  async noema(noemaWhereUniqueInput: Prisma.NoemaWhereUniqueInput): Promise<NoemaDto | null> {
    return this.prisma.noema.findUnique({
      where: noemaWhereUniqueInput,
      include: {
        user: true,
        attachments: true,
        tags: true
      }
    });
  }

  async findNoemaByUid(uid: string): Promise<NoemaDto | null> {
    return this.noema({ uid });
  }

  async noemas(params: {
    skip?: number;
    take?: number;
    cursor?: Prisma.NoemaWhereUniqueInput;
    where?: Prisma.NoemaWhereInput;
    orderBy?: Prisma.NoemaOrderByInput;
  }): Promise<NoemaDto[]> {
    const { skip, take, cursor, where, orderBy } = params;
    return this.prisma.noema.findMany({
      skip,
      take,
      cursor,
      where,
      orderBy,
      include: {
        attachments: true,
        tags: true
      }
    });
  }

  async queryNoema(params: {
    pageable?: Pageable,
    where?: Prisma.NoemaWhereInput;
  }): Promise<ArrayPage<NoemaDto>> {

    const pageable = params.pageable ?? new Pageable();

    this.logger.debug({ op: 'queryNoema', params, pageable });
    const findParams = pageableToPrismaParams(pageable, params.where);

    const content = await this.noemas(findParams);

    const count = await this.prisma.noema.count({ where: params.where });

    this.logger.debug({ op: 'queryNoema', 'result_count': content.length });

    return new ArrayPage(content, count, pageable);
  }

  async createNoema(
    data: Prisma.NoemaCreateInput,
    account: UserAccount,
    params?: { 
      tags?: Prisma.NoemaTagCreateManyNoemaInputEnvelope | string[], 
      attachments?: Prisma.NoemaAttachmentCreateInput[] 
    }
  ): Promise<NoemaDto> {
    data['userUid'] = account.userUid;
    if (params?.tags) {
      data.tags = {
        createMany: this.toNoemaTagsInputEnvelope(params?.tags)
      };
    }
    if (params?.attachments) {
      data.attachments = {
        create: params.attachments
      };
    }
    return this.prisma.noema.create({
      data,
    });
  }

  async updateNoema(
    where: Prisma.NoemaWhereUniqueInput,
    data: NoemaCreateDto,
    params: {
      tags?: Prisma.NoemaTagCreateInput[] | string[],
      attachments?: Prisma.NoemaAttachmentCreateInput[]
    }
  ): Promise<NoemaDto | null> {
    // const { where, data } = params;

    const origNoema = await this.prisma.noema.findUnique({
      where: where,
      include: {
        user: true,
        attachments: true,
        tags: true
      }
    });

    if (origNoema === null) return null; // Should return 404

    // const tags = [...data.tags];  

    const updateTags = params.tags ? this.updateTags(
      origNoema,
      origNoema['tags'],
      this.toNoemaTagsInput(params.tags)
    ) : [];

    const noemaToAdd = data as Prisma.NoemaUpdateInput;
    noemaToAdd.sid = origNoema.sid;
    noemaToAdd.dateCreated = origNoema.dateCreated;
    noemaToAdd.dateModified = new Date();
    noemaToAdd.tags = undefined;
    noemaToAdd.attachments = undefined;
    const updateNoema = this.prisma.noema.update({
      data: noemaToAdd,
      where,
    });

    const txs = [updateNoema, ...updateTags];

    const [updatedNoema] = await this.prisma.$transaction(txs);
    return updatedNoema;
  }

  async deleteNoema(where: Prisma.NoemaWhereUniqueInput): Promise<NoemaDto> {
    return this.prisma.noema.delete({
      where,
    });
  }

  toNoemaTagsInputEnvelope(input: Prisma.NoemaTagCreateManyNoemaInputEnvelope | string[]): Prisma.NoemaTagCreateManyNoemaInputEnvelope {

    if ((input as Prisma.NoemaTagCreateManyNoemaInputEnvelope).data !== undefined) {
      return input as Prisma.NoemaTagCreateManyNoemaInputEnvelope;
    }

    const noemaTags: Prisma.NoemaTagCreateManyNoemaInputEnvelope = {
      data: (input as string[]).map(x => { return { name: x, type: 'tag' } })
    };
    return noemaTags;
  }

  toNoemaTagsInput(input: Prisma.NoemaTagCreateInput[] | string[]): Prisma.NoemaTagCreateInput[] {

    if ((input[0] as Prisma.NoemaTagCreateInput).name !== undefined) {
      return input as Prisma.NoemaTagCreateInput[];
    }

    const noemaTags: Prisma.NoemaTagCreateInput[] = (input as string[]).map(x => { return { name: x, type: 'tag', noema: undefined } })

    return noemaTags;
  }

  updateTags(
    noema: Noema,
    originalTags: Prisma.NoemaTagCreateInput[],
    newTags: Prisma.NoemaTagCreateInput[]
  ): PrismaPromise<Prisma.BatchPayload>[] {
    const tagsDiff = this.tagsDiff(originalTags, newTags);

    const uidsToRemove = tagsDiff.removed.map(x => x.uid);

    let txs: any[] = [];
    if (uidsToRemove.length > 0) {
      const remove = this.prisma.noemaTag.deleteMany({
        where: {
          uid: {
            in: uidsToRemove
          }
        }
      });
      txs.push(remove);
    }

    const tagsToAdd: Prisma.NoemaTagCreateManyInput[] = tagsDiff.added.map(x => { return { noemaUid: noema.uid, type: x.type, name: x.name } });
    if (tagsDiff.added.length > 0) {
      const add = this.prisma.noemaTag.createMany({
        data: tagsToAdd
      });
      txs.push(add);
    }

    return txs;
  }

  contains(arr: Prisma.NoemaTagCreateInput[], el: Prisma.NoemaTagCreateInput): boolean {
    return arr.filter(x => x.name === el.name).length > 0;
  }

  setMinus(a: Prisma.NoemaTagCreateInput[], b: Prisma.NoemaTagCreateInput[]): Prisma.NoemaTagCreateInput[] {
    const result = a.filter(x => !this.contains(b, x));
    return result;
  }

  tagsDiff(originalTags: Prisma.NoemaTagCreateInput[], newTags: Prisma.NoemaTagCreateInput[]) {
    return {
      removed: this.setMinus(originalTags, newTags),
      added: this.setMinus(newTags, originalTags)
    };
  }
}
