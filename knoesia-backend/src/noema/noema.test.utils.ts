import { Prisma } from "@prisma/client";


// Auxiliary functions:
export function newNoema(content: string): Prisma.NoemaCreateInput {
  return {
    kind: 'noema',
    eventDate: new Date(),
    content: content,
    user: undefined
  }
}

export function newNoemaTag(name: string, uid?: string, type: string = 'test'): Prisma.NoemaTagCreateInput {
  return {
    uid,
    name,
    type,
    noema: undefined
  };
}
