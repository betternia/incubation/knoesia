
// From NoemaAttachmentUncheckedCreateInput
export class NoemaAttachmentDto {
  sid?: number
  uid?: string
  type: string
  data: string
  noemaUid?: string | null
}
