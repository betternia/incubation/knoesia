/**
 * From: https://github.com/auth0-blog/wab-menu-api-nestjs/blob/master/src/authz/jwt.strategy.ts
 */
import { Injectable, Logger } from '@nestjs/common';
import { PassportStrategy } from '@nestjs/passport';
import { ExtractJwt, Strategy } from 'passport-jwt';
import { passportJwtSecret } from 'jwks-rsa';

import { Prisma, UserAccount } from '@prisma/client';

import { UserService } from 'src/user/user.service';

@Injectable()
export class JwtStrategy extends PassportStrategy(Strategy) {
  private readonly logger = new Logger(JwtStrategy.name);

  constructor(private readonly userService: UserService) {
    super({
      secretOrKeyProvider: passportJwtSecret({
        cache: true,
        rateLimit: true,
        jwksRequestsPerMinute: 5,
        jwksUri: `${process.env.AUTH0_ISSUER_URL}.well-known/jwks.json`,
      }),

      jwtFromRequest: ExtractJwt.fromAuthHeaderAsBearerToken(),
      audience: process.env.AUTH0_AUDIENCE,
      issuer: `${process.env.AUTH0_ISSUER_URL}`,
      algorithms: ['RS256'],
    });

    console.log('AUTH0_ISSUER_URL:' + process.env.AUTH0_ISSUER_URL);
    console.log('AUTH0_AUDIENCE:' + process.env.AUTH0_AUDIENCE);

  }

  async validate(payload: any): Promise<UserAccount | Prisma.UserAccountCreateInput> {
    this.logger.debug({ 'op': 'validated', payload });
    // retrieve account
    const criteria = {
      source_sourceAccountId: {
        source: payload.iss,
        sourceAccountId: payload.sub
      }
    };

    return await this.userService.account(criteria)
      || {
        source: payload.iss,
        sourceAccountId: payload.sub,
        user: undefined
      };
    // return payload;
  }
}
