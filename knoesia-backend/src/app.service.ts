import { Injectable } from '@nestjs/common';

import * as AppMeta from './app.meta.json'

@Injectable()
export class AppService {
  getInfo(): any {
    return { 
      name: 'Knoesia',
      version: AppMeta.APP_VERSION
    };
  }
}
