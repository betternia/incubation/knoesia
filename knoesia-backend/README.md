Knoesia
=======

## Description

Backed application that manages Noemas. Noema is a unit of thought.

## Running the dependencies (postgres)
```
$ docker run --rm --name kno-postgres -p 5432:5432 -e POSTGRES_USER=knoesia -e POSTGRES_PASSWORD=knoesiapwd -e POSTGRES_DB=knoesia-local -d postgres:12
```

## Installation

```bash
# Install dependencies
$ yarn install

# Run migrations
$ npx prisma migrate deploy --preview-feature 
```

## Running the app

You can run the components manually as bellow, but prefer running using docker-compose, see section below.

```bash
# development
$ yarn start

# watch mode
$ yarn start:dev

# production mode
$ yarn start:prod
```

## Test
> NOTE: For the tests to run, a postgres instance must be running and migrations should have been applied.

```bash
# unit tests
$ yarn test --runInBand

# e2e tests
$ yarn test:e2e

# test coverage
$ yarn test:cov
```

## Build docker image
```bash
# Login to container (fo)
export DOCKER_REGISTRY=registry.gitlab.com
export IMAGE_TAG=${DOCKER_REGISTRY}/betternia/incubation/knoesia/knoesia-backend:latest
docker login ${DOCKER_REGISTRY} -u ${CONTAINER_REGISTRY_USER} -p ${CONTAINER_REGISTRY_TOKEN}

# build
$ docker build -t ${IMAGE_TAG} -f docker/Dockerfile .

# push to registry
$ docker push ${IMAGE_TAG}

# run
$ docker run knoesia-backend -p 8080:3000

# run from latest release
$ docker run ${IMAGE_TAG} -p 8080:3000
```

## Run with docker-compose
```bash
# In dev mode (will listen port 8080, and restart on update)
# The envoy proxy will route path `/api` to knoesia service's root
# Open browser to http://localhost:8080/apidoc/ for swagger
DB_PASSWORD=knoesiapwd docker-compose --env-file .env.docker-test -f docker-compose.local-dev.yml up
# Or
yarn start:indocker:dev

# In test mode (will run the dependent services for e2e tests)
DB_PASSWORD=knoesiapwd docker-compose --env-file .env.docker-test -f docker-compose.test.yml up -d --build
# OR just
yarn deps:up

# Run tests
docker-compose -f docker-compose.test.yml --env-file .env.docker-test run knoesia-backend yarn test -runInBand
# OR just
yarn test:indocker

# Running prod mode locally needs fixing the paths, following line will not work
DB_PASSWORD=knoesiapwd docker-compose --env-file ../.env.docker-local -f docker/docker-compose.yml up
```

To manually run in cloud DEV env
```sh
DB_PASSWORD=knoesiapwd docker-compose --env-file .env.docker -f docker/docker-compose.yml up -d
```

## Useful tools
 To access Postgres DB, you can use Beekeeper Studio

## Support

## License

Knoesia is [MIT licensed](LICENSE).
